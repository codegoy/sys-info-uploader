// +build linux

package main

import (
	"fmt"
	"syscall"
)

// https://github.com/pbnjay/memory
// Copyright (c) 2017, Jeremy Jay
func sysMem() int {
	si := &syscall.Sysinfo_t{}
	err := syscall.Sysinfo(si)
	if err != nil {
		fmt.Printf("ERROR::SYSCALL::%v\n", err)
		return 0
	}
	return ((int(si.Totalram) * int(si.Unit)) / 1024) / 1024
}
